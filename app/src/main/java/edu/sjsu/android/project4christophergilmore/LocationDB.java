package edu.sjsu.android.project4christophergilmore;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

public class LocationDB extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "locationsDatabase";
    private static final int VERSION = 1;
    private static final String TABLE_NAME = "markers";
    protected static final String ID = "_id";
    protected static final String Latitude = "latitude";
    protected static final String Longitude = "longitude";
    protected static final String Zoom_Level = "zoomlevel";
    static final String CREATE_TABLE =
            " CREATE TABLE " + TABLE_NAME +
                    " ("+ ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + Latitude + " DOUBLE NOT NULL, "
                    + Longitude + " DOUBLE NOT NULL, "
                    + Zoom_Level + " FLOAT NOT NULL);";

    public LocationDB(@Nullable Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldV, int newV) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public void delete() {
        //Log.d("DELETE", "GOT HERE TOO");
        SQLiteDatabase database = getWritableDatabase();
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        database.execSQL(CREATE_TABLE);
    }

    public long insert(ContentValues contentValues) {
        SQLiteDatabase database = getWritableDatabase();
        return database.insert(TABLE_NAME, null, contentValues);
    }

    public Cursor getAllMarkers(String orderBy) {
        SQLiteDatabase database = getWritableDatabase();
        return database.query(TABLE_NAME,
                new String[]{ID, Latitude, Longitude, Zoom_Level},
                null, null, null, null, orderBy);
    }
}
