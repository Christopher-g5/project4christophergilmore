package edu.sjsu.android.project4christophergilmore;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LoaderManager.LoaderCallbacks<Cursor> {
    private final LatLng LOCATION_UNIV = new LatLng(37.335371, -121.881050);
    private final LatLng LOCATION_CS = new LatLng(37.333714, -121.881860);
    private final LatLng LOCATION_CITY = new LatLng(37.3183318, -121.9510491);
    private final String AUTHORITY = "edu.sjsu.android.project4christophergilmore";
    private final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);
    private GoogleMap mMap;
    private Button cityButton;
    private Button universityButton;
    private Button csButton;
    private FloatingActionButton locationButton;
    private FloatingActionButton uninstallButton;
    private Context context1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        context1 = this;
        cityButton = findViewById(R.id.cityButton);
        cityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchView(v);
            }
        });
        universityButton = findViewById(R.id.universityButton);
        universityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchView(v);
            }
        });
        csButton = findViewById(R.id.csButton);
        csButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchView(v);
            }
        });
        locationButton = findViewById(R.id.fabLocation);
        locationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLocation(v);
            }
        });
        uninstallButton = findViewById(R.id.fabUninstall);
        uninstallButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uninstallClick(v);
            }
        });


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);



        //restarts cursorLoader on orientation change
        LoaderManager.getInstance(this).restartLoader(0, null, this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {
                //mMap.clear();
                mMap.addMarker(new MarkerOptions().position(point));
                //Add point to database
                new AsyncTaskInsert(point,mMap.getCameraPosition().zoom).execute();
                //addMarker(point.longitude, point.latitude, mMap.getCameraPosition().zoom);
                //getAllMarkers();
                Toast.makeText(context1, "Marker Added", Toast.LENGTH_SHORT).show();
            }
        });

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                mMap.clear();
                new AsyncTaskDelete().execute();
                //getAllMarkers();
            }
        });

        //Initialize cursor loader when map is ready
        LoaderManager.getInstance(this).initLoader(0, null, this);
    }

    public void switchView(View view) {
        CameraUpdate update = null;
        if (view.getId() == R.id.cityButton) {
            mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            update = CameraUpdateFactory.newLatLngZoom(LOCATION_CITY, 10f);
        } else if (view.getId() == R.id.universityButton) {
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            update = CameraUpdateFactory.newLatLngZoom(LOCATION_UNIV, 14f);
        } else if (view.getId() == R.id.csButton) {
            mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
            update = CameraUpdateFactory.newLatLngZoom(LOCATION_CS, 18f);
        }CameraUpdateFactory.newLatLngZoom(LOCATION_CS, 18f);
        mMap.animateCamera(update);
    }

    public void getLocation(View view){
        GPSTracker tracker = new GPSTracker(this);
        tracker.getLocation();
    }

    public void addMarker(double longitude, double latitude, float zoomLevel) {
        ContentValues values = new ContentValues();
        values.put("longitude", longitude);
        values.put("latitude", latitude);
        values.put("zoomlevel", zoomLevel);

        getContentResolver().insert(CONTENT_URI, values);
//        Toast.makeText(this, "Marker Added", Toast.LENGTH_SHORT).show();
    }

    public void clearMarkers() {
        getContentResolver().delete(CONTENT_URI, null, null);
    }

    public void restoreMarkers(Cursor cursor){
        if(cursor.moveToFirst()) {
            LatLng point = new LatLng(cursor.getDouble(1), cursor.getDouble(2));
            mMap.addMarker(new MarkerOptions().position(point));
            while (cursor.moveToNext()) {
                point = new LatLng(cursor.getDouble(1), cursor.getDouble(2));
                mMap.addMarker(new MarkerOptions().position(point));
            }
        }else{

        }
    }

    public void getAllMarkers() {
        // Sort by student name
        try (Cursor c = getContentResolver().
                query(CONTENT_URI, null, null, null, null)) {
            if (c.moveToFirst()) {
                String result = "Chris's Coordinates: \n";
                do {
                    for (int i = 0; i < c.getColumnCount(); i++) {
                        result = result.concat(c.getString(i) + "\t");
                    }
                    result = result.concat("\n");
                } while (c.moveToNext());
                //Log.d("MARKERS", result);
            }
        }
    }

    public void uninstallClick(View view){
        Intent delete = new Intent(Intent.ACTION_DELETE, Uri.parse("package:" + getPackageName())); startActivity(delete);
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
        CursorLoader cursorLoader = new CursorLoader(this, CONTENT_URI, null,
                null, null, null);
        //Log.d("CURSOR", "Started");
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor data) {
        restoreMarkers(data);
        //Log.d("CURSOR", "FINISHED");
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {

    }

    private class AsyncTaskInsert extends AsyncTask<Void, Void, Void>{
        private LatLng point;
        private float zoom;

        public AsyncTaskInsert(LatLng point, float zoom) {
            this.point = point;
            this.zoom = zoom;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            addMarker(point.longitude, point.latitude, zoom);
            return null;
        }
    }

    private class AsyncTaskDelete extends AsyncTask<Void, Void, Void>{
        @Override
        protected Void doInBackground(Void... voids) {
            clearMarkers();
            return null;
        }
    }


}